#!/usr/bin/env bash
set -euo pipefail
trap 'echo "Aborting due to errexit on line $LINENO. Exit code: $?" >&2' ERR
IFS=$'\n\t'

sudo dnf update
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh" | sudo bash
sudo dnf install -y gitlab-runner openscap openscap-scanner scap-security-guide openscap-utils podman
sudo podman pull registry.gitlab.com/gitlab-org/build/cng/gitlab-base:master-fips
